package com.example.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.app2.databinding.ActivityAnsfragmentBinding
import android.util.Log
import androidx.navigation.findNavController
class Ansfragment : Fragment() {
    private var _binding: ActivityAnsfragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var result: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        result = arguments?.getString(RESULT).toString()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityAnsfragmentBinding.inflate(inflater, container, false)
        binding.resultText.text = result
        val view = binding.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.restartButton.setOnClickListener {
            val action = AnsfragmentDirections.actionAnsFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }
    companion object {
        const val RESULT = "result"
    }
}
