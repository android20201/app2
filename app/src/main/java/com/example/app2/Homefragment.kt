package com.example.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.app2.databinding.ActivityHomefragmentBinding


class HomeFragment : Fragment() {
    private var _binding: ActivityHomefragmentBinding? = null

    private val binding get() = _binding!!



    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

}

override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
): View? {
    _binding = ActivityHomefragmentBinding.inflate(inflater, container, false)
    val view = binding.root
    return view
}

override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    binding.PlusButton.setOnClickListener {
        val action = HomeFragmentDirections.actionHomeFragmentToPlusFragment()
        view.findNavController().navigate(action)
    }
    binding.minusButton.setOnClickListener {
        val action = HomeFragmentDirections.actionHomeFragmentToMinusFragment()
        view.findNavController().navigate(action)
    }
}

override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
}
companion object {
}
}
