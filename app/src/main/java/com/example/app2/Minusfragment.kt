package com.example.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.app2.databinding.ActivityMinusfragmentBinding

class MinusFragment : Fragment() {
    private var _binding: ActivityMinusfragmentBinding? = null
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityMinusfragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.equalButton.setOnClickListener {
            var num1 = binding.num1.text.toString().toInt()
            var num2 = binding.num2.text.toString().toInt()
            var result = num1-num2
            val action = MinusFragmentDirections.actionMinusFragmentToAnswerFragment(result = result.toString())
            view.findNavController().navigate(action)
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }

    companion object {


    }
}
